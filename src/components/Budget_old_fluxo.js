import * as d3 from "d3";
import { sankey, sankeyLinkHorizontal, sankeyCenter } from "d3-sankey";
import React from 'react';
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

function BudgetSankey(){
  let w = window.innerWidth,
      h = window.innerHeight*0.8;
  let svg;
  let margin = {top: 10, left:0, bottom: 10, right:0};
  let states = ["sp"];
  let budgetData = {};
  let currState = 0;
  let currYear = 2018;

  function setup(){

    svg = d3.select("#svg-budget-1");

    // load national gender data
    d3.tsv("data/orcamento-subfuncao.tsv", function(d,di,cols){
      for (var i = 0; i < cols.length; ++i){
        if(i > 3){ d[cols[i]] = +d[cols[i]]; }
      }
      return d;
    })
    .then(function(data) {
      let filtered = d3.nest()
      .key(function(d){
        return d.uf.toLowerCase()+"_"+d.ano;
      })
      .entries(data);
      budgetData = {};

      filtered.forEach(function(d,i){
        var values = d.values;
        budgetData[d.key] = {"nodes" : [], "links" : []};

        /*
        values = values.sort(function(a, b){
           return d3.ascending(a.funcao, b.funcao);
        })
        */

        for(var i = 0; i<values.length; i++){
          budgetData[d.key].nodes.push({ "name": values[i].funcao });
          budgetData[d.key].nodes.push({ "name": values[i].subfuncao });
          budgetData[d.key].links.push({ "source": values[i].funcao,
                                        "target": values[i].subfuncao,
                                        "value": +values[i].valor_emp });
        }

        // only unique nodes
        budgetData[d.key].nodes = d3.keys(d3.nest()
          .key(function (dd) { return dd.name; })
          .object(budgetData[d.key].nodes));

        // replacing the text of each link with its index from node
        budgetData[d.key].links.forEach(function (dd, di) {
          budgetData[d.key].links[di].source = budgetData[d.key].nodes.indexOf(budgetData[d.key].links[di].source);
          budgetData[d.key].links[di].target = budgetData[d.key].nodes.indexOf(budgetData[d.key].links[di].target);
        });

        budgetData[d.key].nodes.forEach(function (dd, di) {
          budgetData[d.key].nodes[di] = { "name": dd };
        });
      });

      draw();
    })
    /*.catch(function(error){
       console.log("error on 'budget-1'");
    });*/
  }

  function draw(){
    let currKey = states[currState] + "_" + currYear;
    let chart, chartW, chartH, svgSankey;

    w = Number(svg.style("width").slice(0, -2));
    h = window.innerHeight*.75;//svg.style("height").slice(0, -2);
    if(window.innerHeight < w*0.8 && h < 600){
      h = 600;
    }

    svg.attr("viewBox", "0 0 "+ w +" "+h);

    /*
    margin = {top: 10, left:130, bottom: 10, right:130};
    if(w <= 680){
      margin = {top: 10, left:0, bottom: 10, right:0};
    }*/

    chartW = w - margin.left - margin.right;
    chartH = h - margin.top - margin.bottom;

    const { nodes, links } = sankey()
    .nodeSort(null)
    .linkSort(null)
    .nodeAlign(sankeyCenter)
    .nodeWidth(10)
    .nodePadding(15)
    .extent([[1, 1], [chartW, chartH]])(budgetData[currKey]);

    svgSankey = svg.append("g")
      .attr("transform", "translate("+margin.left+",0)");

    let budgetLinks = svgSankey.append("g")
      .selectAll(".link")
      .data(links)
      .enter()
      .append("path")
      //.classed("link", true)
      .attr("class", function(d){
        let groupClass = " path_"+d.source.index;
        return "link"+groupClass;
      })
      .attr("d", sankeyLinkHorizontal())
      .attr("fill", "none")
      .attr("stroke", "#606060")
      .attr("stroke-width", function(d){
        let stw = d.width;
        if(stw < 1){stw = 1}
        return stw;
      })
      .attr("stoke-opacity", 0.5)
      .on("mouseover click", function(d){
        //console.log(d);
        let sourceClass = ".path_"+d.source.index
        + ",.grp_"+d.source.index
        + ",.label_"+d.source.index;
        d3.selectAll(".link, .node, .label").classed("active", false)
          .classed("inactive", true)
        d3.selectAll(sourceClass).classed("active", true)
          .classed("inactive", false)
      })
      .on("mouseout", function(d){
        d3.selectAll(".link, .node, .label")
          .classed("active", false)
          .classed("inactive", false)
      });

    let budgetGroups = svgSankey.append("g");
    budgetGroups.classed("nodes", true)
      .selectAll("rect")
      .data(nodes)
      .enter()
        .append("rect")
        .attr("class", function(d){
          let tgt = d.targetLinks[0] || d.sourceLinks[0];
          let groupClass = " grp_"+tgt.source.index;
          return "node"+groupClass;
        })
        .attr("x", d => d.x0)
        .attr("y", d => d.y0)
        .attr("width", d => d.x1 - d.x0)
        .attr("height", d => d.y1 - d.y0 + 1)
        .on("mouseover click", function(d){
          let tgt = d.targetLinks[0] || d.sourceLinks[0];
          let sourceClass = ".path_"+tgt.source.index
          +",.grp_"+tgt.source.index
          +",.label_"+tgt.source.index;

          d3.selectAll(".link, .node, .label").classed("active", false)
            .classed("inactive", true)
          d3.selectAll(sourceClass).classed("active", true)
            .classed("inactive", false)
        })
        .on("mouseout", function(d){
          d3.selectAll(".link, .node, .label")
            .classed("active", false)
            .classed("inactive", false)
        });

    let budgetLabels = svg.append("g")
      .attr("transform", "translate("+(margin.left - 10)+",0)");
    budgetLabels.selectAll("text")
      .data(nodes)
      .enter()
        .append("text")
        .attr("class", function(d){
          let tgt = d.targetLinks[0] || d.sourceLinks[0];
          let groupClass = " label_"+tgt.source.index;
          if(d.sourceLinks[0]){
            groupClass += " visible";
          }
          return "label"+groupClass;
        })
        .attr("x", function(d){
          let xPos = d.x0 + 40;
          let tgt = d.targetLinks[0];
          if(d.targetLinks[0]){
            xPos = d.x0;
          }
          return xPos
        })
        .attr("y", d => d.y0 + (d.y1 - d.y0)/2 + 5)
        .attr("width", d => d.x1 - d.x0)
        .attr("height", d => d.y1 - d.y0)
        .attr("fill", "#dedede")
        .attr("font-size", function(){
          let fSize = "10px";
          if(w < 600){
            fSize = "9px";
          }
          return fSize;
        })
        .attr("font-family", "Raleway")
        .attr("text-anchor", function(d){
          let xAnchor = "end";
          if(d.sourceLinks[0]){
            xAnchor = "start";
          }
          return xAnchor;
        })
        .on("mouseover click", function(d){
          let tgt = d.targetLinks[0] || d.sourceLinks[0];
          let sourceClass = ".path_"+tgt.source.index
          +",.grp_"+tgt.source.index
          +",.label_"+tgt.source.index;

          d3.selectAll(".link, .node, .label").classed("active", false)
            .classed("inactive", true)
          d3.selectAll(sourceClass).classed("active", true)
            .classed("inactive", false)
        })
        .on("mouseout", function(d){
          d3.selectAll(".link, .node, .label")
            .classed("active", false)
            .classed("inactive", false)
        })
        .text(d => d.name);

  }

  window.addEventListener("resize", function(){
    //draw();
  });
  let initTimeout = setTimeout(setup, 200);

  return (
    <section id="budget">
    <h3>Orçamento por Função e Subfunção</h3>
    <div id="budget-intro" className="intro-block">
    </div>
    <div className="chart-block">
      <svg id="svg-budget-1" preserveAspectRatio="xMinYMin meet"  viewBox={"0 0 "+w+" "+h}></svg>
    </div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    </section>
  )
}

export default BudgetSankey;
