import * as d3 from "d3";
import { sankey, sankeyLinkHorizontal, sankeyCenter } from "d3-sankey";
import React from 'react';
import Select from './custom.select';
import BudgetFunc from './BudgetFuncPack';
//import Budget1B from './Budget1B';
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

function Budget1(){
  let w = window.innerWidth,
      h = window.innerHeight*.8;
  let vis3Pos = {x:0, y: 0};
  let vis2Pos = [];
  let svg, vis1, vis2, vis3, parent, path, label;
  let margin = {top: 10, left:0, bottom: 10, right:0};
  let radius = w / 6;
  let budgetData, dataTotalBudget, dataSumBudget, budgetByFunction;
  let currVars = {state: 0, year: 0, value:3};
  let vars = {states: ["ce", "pr", "sp"],
              values: ["valor_loa", "valor_sup", "valor_atual", "valor_emp"],
              value_names: ["previsto", "suplementado", "atualizado", "executado"],
              years: ["2018","2017","2016","2015","2014","2013"]};
  let nodes, arc, partition;
  let xAxis;
  let format;
  let currencyFormat = d3.format("($.2f");
  let tooltip, returnBtn;
  let selectors = [];
  let currStep = 1, maxPoints = 4;
  let currSelection, currUFYear;
  let chartWidth = (w - 80)/2;
  let colors = {
    "LEGISLATIVA": "#3A8DA0",
    "JUDICIÁRIA": "#5AB755",
    "ESSENCIAL À JUSTIÇA": "#D33145",
    "SEGURANÇA PÚBLICA": "#F7D841",
    "DIREITOS DA CIDADANIA": "#AE397C"
  };

  let hashFills = {
    "LEGISLATIVA": "url(#diagCL)",
    "JUDICIÁRIA": "url(#diagCJ)",
    "ESSENCIAL À JUSTIÇA": "url(#diagCE)",
    "SEGURANÇA PÚBLICA": "url(#diagCS)",
    "DIREITOS DA CIDADANIA": "url(#diagCD)"
  };

  function setup(){
    format = d3.format(",d");
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip")
    }

    let customSelects = document.querySelector("#budget").querySelectorAll(".select");

    selectors = [];
    customSelects.forEach(function(e,i){
      selectors.push( new Select({target: e, callback: changeSelections}) );
    });

    svg = d3.select("#svg-budget-1");

    partition = data => {
      const root = d3.hierarchy(data)
          .sum(function(d){
            return d[vars.values[currVars.value]]
          })
          .sort(function(a, b){
            return b.data.valor_emp - a.data.valor_emp
          });
      return d3.partition()
          .size([2 * Math.PI, root.height + 1])
        (root);
    }
    arc = d3.arc()
    .startAngle(d => d.x0)
    .endAngle(d => d.x1)
    .padAngle(d => Math.min((d.x1 - d.x0) / 2, 0.005))
    .padRadius(radius * 1.5)
    .innerRadius(d => d.y0 * radius)
    .outerRadius(d => Math.max(d.y0 * radius, d.y1 * radius - 1))

    // load national gender data
    d3.tsv("data/orcamento-subfuncao.tsv", function(d,di,cols){
      for (var i = 0; i < cols.length; ++i){
        if(i > 3){ d[cols[i]] = +d[cols[i]]; }
      }
      return d;
    })
    .then(function(data) {
      // soma as linhas com a mesma "função - subfunção"
      let uniqueData = d3.nest()
        .key(function(d) {
          return d.ano+" - "+d.funcao+" - "+d.subfuncao+" - "+d.uf;
        })
        .rollup(function(v) {
          let obj = { uf: v[0].uf, ano: v[0].ano, funcao: v[0].funcao, subfuncao: v[0].subfuncao};
          for(var i=0; i<vars.values.length; i++){
            obj[vars.values[i]] = d3.sum(v, function(g) {return g[vars.values[i]]; }).toFixed(2);
          }
          return obj;
        }).entries(data);

      let unfiltered = [];
      uniqueData.forEach(function(d,i){
        unfiltered.push(d.value);
      });

      let filtered = d3.nest()
      .key(function(d){
        return d.uf.toLowerCase()+"_"+d.ano;
      })
      .entries(unfiltered);
      //console.log(filtered);
      budgetData = {};
      filtered.forEach(function(d,i){
        budgetData[d.key] = d.values;
      });
      //console.log(budgetData);
      d3.tsv("data/orcamento-total.tsv")
      .then(function(tData){
        let bFiltered = d3.nest()
        .key(function(d){
          return d.uf.toLowerCase()+"_"+d.ano;
        })
        .entries(tData);

        dataTotalBudget = {};
        bFiltered.forEach(function(d,i){
          dataTotalBudget[d.key] = d.values;
        });

        let bSums = d3.nest()
          .key(function(d) {
            return d.ano+" - "+d.uf.toLowerCase();
          })
          .rollup(function(v) {
            let obj = { uf: v[0].uf, ano: v[0].ano};
            for(var i=0; i<vars.values.length; i++){
              obj[vars.values[i]] = d3.sum(v, function(g) {return g[vars.values[i]]; }).toFixed(2);
            }
            return obj;
          }).entries(data);

        dataSumBudget = {};
        bSums.forEach(function(d,i){
          dataSumBudget[d.key] = [d.value];
        });

        draw(currStep);
      })
    });

    // how many step points?
    maxPoints = d3.select("#budget1-intro").select(".text-block")
      .selectAll("p").size();

    d3.selectAll("#next-budget,#last-budget").on("click", function(d){
      let isNotLast = currStep < maxPoints;
      let increment = 1;
      let initPart = 1;

      if(d3.select(this).attr("id")=="last-budget"){
        isNotLast = currStep > 1;
        increment = -1;
        initPart = maxPoints;
      }

      if(isNotLast){
        currStep += increment;
        draw(currStep);
      }else{
        //currStep = initPart;
        //draw(currStep);
      }

      let stepper = d3.select("#budget-stepper");
      stepper.style("opacity", 0.7);
      stepper.selectAll("li").each(function(d, i){
        if(i!=currStep){
          d3.select(this).classed("active", false);
          d3.select("#budget1-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", false);
        }else{
          d3.select(this).classed("active", true);
          d3.select("#budget1-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", true);
        }
      })
    });
  }

  function draw(step, redraw){
    w = d3.select("#budget1-container").style("width").slice(0, -2);
    h = window.innerHeight*.8;
    if(h < 400 || window.innerHeight > window.innerWidth){ h = 400; };
    if(h > 580){ h = 580; };
    radius = (h-60) / 6;

    svg.attr("viewBox", "0 0 "+ w +" "+h);

    currSelection = vars.states[currVars.state] +
                    "_"+vars.years[currVars.year];

    currUFYear =  vars.years[currVars.year] +
                  " - "+vars.states[currVars.state];


    let maxValue = d3.max(dataTotalBudget[currSelection], function(d){
      return +d.valor_emp;
    });

    let prop = vars.values[currVars.value];
    let circScale = d3.scaleSqrt().range([0,radius*3]).domain([0,maxValue]);
    let circTotalV = dataTotalBudget[currSelection][0][prop];
    let circSumV = dataSumBudget[currUFYear][0][prop];
    // a razão do círculo menor com relação ao maior
    let circRatio = circScale(circSumV)/circScale(circTotalV);
    let duration = 500;

    // first visualization
    if(step == 1){
      d3.select("#budget1-leg").classed("active",false);
      d3.select("#bdg-select-values").classed("hidden",true);

      if(!vis1 || redraw){
        buildVis1(circRatio, circScale);
      }else{
        vis1.attr("opacity", 0).style("display", "block");
        vis1.transition().delay(300).duration(duration)
        .attr("opacity", 1);
      }

      vis1.select(".bdg-c2")
        .select("circle")
        .transition().duration(300)
        .attr("fill","url(#diagBar)")
        //.attr("fill",colors["SEGURANÇA PÚBLICA"])
    }else{
      d3.select("#budget1-leg").classed("active",true);

      if(vis1){
        vis1.select(".bdg-c2")
          .select("circle")
          .transition().duration(300)
          .attr("fill","#222")
        vis1.transition().delay(600).duration(300)
          .attr("opacity", 0).style("display", "none")
      }
    }

    // second visualization
    if(step == 2){
      d3.select("#bdg-select-values").classed("hidden",false);

      if(!vis2 || redraw){
        buildVis2(circRatio);
      }
      if(!redraw){
        vis2.attr("display", "block")
        vis2.transition().duration(200)
        .attr("opacity",1)

        vis2.transition().delay(500).duration(1000)
        .attr("transform", "translate(" + (w/2) + "," + (h/2) + ") scale(1)");
      }else{
        vis2
        .attr("transform", "translate(" + (w/2) + "," + (h/2) + ") scale(1)");
      }
    }else if(step != 2){
      if(vis2){
        vis2.transition().delay(0).duration(duration)
        .attr("transform", "translate(" + (w/2) + "," + (h/2) + ") scale("+circRatio+")")
        .attr("opacity", 0)
        .attr("display", "none")
      }
    }

    if(step == 3 || step == 4){
      d3.select("#bdg-select-values").classed("hidden",true);

      if(!vis3 || redraw){
        buildVis3();
        duration = 0;
        vis3.attr("opacity", 1)
        .attr("display","block");
      }else{
        //vis3.attr("opacity", 0);
        duration = 500;
        vis3.transition().delay(100).duration(duration)
        .attr("opacity", 1)
        .attr("display","block");
      }

      //legendas do gráfico de barras
      let barH = (h-60)/10;
      let bdgBarLeg = [];
      let bdgInfo = []
      if(currStep==3){
        bdgInfo = [{fill:"url(#coloredLeg)", text:"Empenhado", col:0},
        {fill:"url(#diagBar)", text:"Previsto (LOA)", col:1}]
      }else{
        bdgInfo = [{fill:"url(#diagBar)", text:"Suplementado", col:0}]
      }

      let bdgW = vis3.node().getBBox().width;
      let colX,lineY;

      vis3.selectAll(".leg").remove();

      for(var i=0; i<bdgInfo.length; i++){
        colX = i*(w/4);
        lineY = (barH*10 + 30);
        if(window.innerWidth < 480){
          colX = bdgInfo[i].col*(w/2);
          lineY = (Math.floor(i/2)*30) + (barH*10 + 30);
        }

        bdgBarLeg[i] = vis3.append("g")
        .attr("class", "leg leg"+(i+1))
        //.attr("transform", "translate(0,"+lineY+")");
        .attr("transform", "translate("+colX+"," + lineY + ")")

        bdgBarLeg[i].append("rect")
          .attr("width", 15)
          .attr("height", 15)
          .attr("rx", 3)
          .attr("ry", 3)
          .attr("fill", bdgInfo[i].fill);

        bdgBarLeg[i].append("text")
          .attr("x", 30)
          .attr("y", 12)
          .attr("fill", "#fff")
          .attr("class", "bdgBarLeg")
          .text(bdgInfo[i].text)
      }

      let max1 = d3.max(budgetByFunction, function(d){
        return +d.valor_loa;
      });
      let max2 = d3.max(budgetByFunction, function(d){
        return +d.valor_emp;
      });

      let divisor = 1000000000;
      let xBarScale = d3.scaleLinear().domain([0,d3.max([max1,max2])]).range([0,(w-5)]);
      xAxis = d3.axisBottom().scale(xBarScale);
      xAxis.tickFormat(function(d){return (d/divisor).toFixed(1)});
      vis3.select(".axis-title")
      .text("R$ bilhões")

      vis3.select(".axis-x")
          .transition().duration(duration)
          .call(xAxis)

      if(step == 3){
        vis3.selectAll(".barSup")
        .transition().duration(duration)
        .attr("width", 0)
        .attr("x", function(d,i){
          let xPos = xBarScale(d.valor_emp);
          return xPos;
        })

        vis3.selectAll(".barEmp")
        .transition().duration(duration)
        .attr("width", function(d){
          return xBarScale(d.valor_emp);
        })
        .attr("x", xBarScale(0))

        vis3.selectAll(".barLoa1")
        .transition().duration(duration)
        .attr("x1", function(d){
          return xBarScale(d.valor_loa);
        })
        .attr("x2", function(d){
          return xBarScale(d.valor_loa);
        });

        vis3.selectAll(".barLoa2")
        .transition().duration(duration)
        .attr("x1", function(d){
          return xBarScale(0);
        })
        .attr("x2", function(d){
          return xBarScale(d.valor_loa);
        })
      }else if(step == 4){
        let maxSup = d3.max(budgetByFunction, function(d){
          return +d.valor_sup;
        });
        let minSup = d3.min(budgetByFunction, function(d){
          return +d.valor_sup;
        });

        if(minSup > 0){
          minSup = 0;
        }

        console.log(minSup,maxSup);
        divisor = 1000000;
        xBarScale = d3.scaleLinear().domain([minSup,maxSup]).range([0,(w-5)]);
        xAxis = d3.axisBottom().scale(xBarScale);
        xAxis.tickFormat(function(d){return (d/divisor).toFixed(1)});
        vis3.select(".axis-title")
        .text("R$ milhões")

        vis3.select(".axis-x")
            .transition().duration(duration)
            .call(xAxis)

        vis3.selectAll(".barSup")
        .transition().duration(duration)
        .attr("width", function(d){
          console.log(xBarScale(0));
          let wSup = xBarScale(d.valor_sup) - xBarScale(0);
          if(d.valor_sup < 0){
            wSup = xBarScale(0) - xBarScale(d.valor_sup);
          }
          return wSup;
        })
        .attr("x", function(d,i){
          let xPos = 0;
          if(d.valor_sup > 0){
            xPos = xBarScale(0);
          }else{
            xPos = -xBarScale(d.valor_sup);
          }
          return xPos;
        })

        vis3.selectAll(".barEmp")
        .transition().duration(duration)
        .attr("width", function(d){
          return 0;
        })
        .attr("x", xBarScale(0))

        vis3.selectAll(".barLoa1")
        .transition().duration(duration)
        .attr("x1", xBarScale(0))
        .attr("x2", xBarScale(0))

        vis3.selectAll(".barLoa2")
        .transition().duration(duration)
        .attr("x1", xBarScale(0))
        .attr("x2", xBarScale(0))
      }

    }else if(step != 3 && step != 4){
      if(vis3){
        vis3.transition().duration(duration)
        .attr("opacity", 0)
        .attr("display", "none")
      }
    }
  }

  function changeSelections(){
    //console.log(selectors);
    selectors.forEach(function(s,i){
      //console.log(s.node.options);
      let sel = s.node;
      let selStr = sel.options[sel.selectedIndex].value;
      let list = sel.parentNode.getAttribute("id").split("-")[2];
      let index = vars[list].indexOf(selStr.toLowerCase());

      currVars[list.substr(0,list.length-1)] = index;
    })

    let tempSelection = vars.states[currVars.state] +
                    "_"+vars.years[currVars.year];

    if(!budgetData[tempSelection]){
      let msg = "<span>Não há dados disponíveis para o estado <b>"+vars.states[currVars.state].toUpperCase()+"</b> em <b>"+vars.years[currVars.year]+"</b>. Por favor, selecione outro ano ou estado</span>";
      d3.select("#bdg1-msg").html(msg).classed("active", true);
    }else{
      d3.select("#bdg1-msg").html("").classed("active", false);
      draw(currStep, true);
    }



  }

  /*function clicked(p) {
    //parent.datum(p.parent || nodes);
    if(p && p.depth == 2){
      p = p.parent;
    }else if(p && p.parent){
      parent.datum(p.parent);
    }else{
      parent.datum(nodes);
    }

    if(p && p.parent){
      d3.select("#budget1-return").classed("active", true);
    }else{
      d3.select("#budget1-return").classed("active", false);
    }

    nodes.each(d => d.target = {
      x0: Math.max(0, Math.min(1, (d.x0 - p.x0) / (p.x1 - p.x0))) * 2 * Math.PI,
      x1: Math.max(0, Math.min(1, (d.x1 - p.x0) / (p.x1 - p.x0))) * 2 * Math.PI,
      y0: Math.max(0, d.y0 - p.depth),
      y1: Math.max(0, d.y1 - p.depth)
    });

    // Transition the data on all arcs, even the ones that aren’t visible,
    // so that if this transition is interrupted, entering arcs will start
    // the next transition from the desired position.
    path.transition(t)
      .tween("data", d => {
        const i = d3.interpolate(d.current, d.target);
        return t => d.current = i(t);
      })
      .filter(function(d) {
        return +this.getAttribute("fill-opacity");// || arcvisible(d.target);
      })
      .attr("fill-opacity", function(d){
        let op,sum;
        if(!d.data.subfuncao){
          op = 1;
        }else{
          sum = d3.sum(d.parent.children, function(dd){ return dd.data[vars.values[currVars.value]];});
          op = 0.5 + ((d.data[vars.values[currVars.value]]/sum)*0.5);
        }
        return op;
      })
      .attrTween("d", d => () => arc(d.current));
  }*/

  function buildVis1(circRatio, circScale){
    let prop = vars.values[currVars.value];
    d3.select("#bdg-vis1").remove();
    vis1 = svg.append("g")
          .attr("id","bdg-vis1")
          .attr("opacity", 0);

    let c1 = vis1.selectAll(".bdg-c1")
    .data(dataTotalBudget[currSelection])
    .enter()
    .append("g")
    .attr("class", "bdg-c1");
    c1.append("circle")
    .attr("cx", (w/2))
    .attr("cy", (h/2))
    .attr("fill","#deded9")
    //.attr("fill","url(#diagBar)")
    .attr("opacity",0.25)
    .attr("r", function(d){
      return Math.round(circScale(d[prop]));
    })
    c1.append("text")
    .attr("x", (w/2))
    .attr("y", (h/2) - radius*3 + 15)
    .style("font-size", "21px")
    .style("font-weight", "700")
    .style("font-family", "Helvetica, Arial, sans-serif")
    .attr("text-anchor","middle")
    .attr("fill","#fff")
    .text(function(d){
      return (Math.round(d[prop])/1000000000).toFixed(1) + " bilhões";
    })
    c1.append("text")
    .attr("x", (w/2))
    .attr("y", (h/2)  - radius*3 + 35)
    .style("font-size", "13px")
    .style("font-weight", "400")
    .style("font-family", "Helvetica, Arial, sans-serif")
    .attr("text-anchor","middle")
    .attr("fill","#fff")
    .text(function(){
      return "TOTAL "+ vars.value_names[currVars.value].toUpperCase();
    })
    c1.append("line")
    .attr("stroke-width", 2)
    .attr("stroke", "#fff")
    .attr("x1", (w/2))
    .attr("y1", (h/2) - radius*3 + 45)
    .attr("x2", (w/2))
    .attr("y2", (h/2) - radius*2 + 25)
    c1.append("circle")
    .attr("cx", (w/2))
    .attr("cy", (h/2) - radius*2 + 25)
    .attr("fill", "#fff")
    .attr("r", 3)


    let c2 = vis1.selectAll(".bdg-c2")
    .data(dataSumBudget[currUFYear])
    .enter()
    .append("g")
    .attr("class", "bdg-c2");
    c2.append("circle")
    .attr("cx", (w/2))
    .attr("cy", (h/2))
    .attr("fill","url(#diagBar)")
    //.attr("fill",colors["SEGURANÇA PÚBLICA"])//url(#diagCS)
    .attr("fill-opacity",.8)
    .attr("r", function(d){
      return Math.round(circScale(d[prop]));
    });

    c2.append("text")
    .attr("x", (w/2))
    .attr("y", (h/2) + radius + 60)
    .style("font-size", "21px")
    .style("font-weight", "700")
    .style("font-family", "Helvetica, Arial, sans-serif")
    .attr("text-anchor","middle")
    .attr("fill","#fff")
    .text(function(d){
      return (Math.round(d[prop])/1000000000).toFixed(1) + " bilhões";
    })
    c2.append("text")
    .attr("x", (w/2))
    .attr("y", (h/2) + radius + 80)
    .style("font-size", "13px")
    .style("font-weight", "400")
    .style("font-family", "Helvetica, Arial, sans-serif")
    .attr("text-anchor","middle")
    .attr("fill",colors["SEGURANÇA PÚBLICA"])
    .text("5 FUNÇÕES")
    c2.append("line")
    .attr("stroke-width", 2)
    .attr("stroke", "#fff")
    .attr("x1", (w/2))
    .attr("y1", (h/2) + 20)
    .attr("x2", (w/2))
    .attr("y2", (h/2) + radius + 30)
    c2.append("circle")
    .attr("cx", (w/2))
    .attr("cy", (h/2) + 20)
    .attr("fill", "#fff")
    .attr("r", 3)

    vis1.transition().duration(300).attr("opacity", 1);
  }

  function buildVis2(circRatio){
    let nestedData = d3.nest()
    .key(function(d){
      return d.funcao;
    })
    .entries(budgetData[currSelection]);

    let json = {funcao: "Funções", children: [] };
    nestedData.forEach(function(d,i){
      let size = d3.sum(d.values, function(dd){
        return dd[vars.values[currVars.value]]
      })
      json.children.push({funcao: d.key, children: d.values });
    });

    nodes = partition(json);
    nodes.each(d => d.current = d);

    svg.select("#budget1-vis2").remove();

    vis2 = svg.append("g")
      .attr("id", "budget1-vis2")
      .attr("transform", "translate(" + (w/2) + "," + (h/2) + ") scale("+circRatio+")");

    parent = vis2.append("circle")
      .datum(nodes)
      .attr("r", radius)
      .attr("fill", "none")
      .attr("pointer-events", "all")
      .style("cursor", "pointer")

    path = vis2.selectAll("path")
      .data(nodes.descendants().slice(1))
      .join("path")
        .attr("fill", d => { while (d.depth > 1) d = d.parent; return colors[d.data.funcao]; })
        .attr("fill-opacity", function(d){
          let op,sum;
          if(!d.data.subfuncao){
            op = 1;
          }else{
            sum = d3.sum(d.parent.children, function(dd){ return dd.data[vars.values[currVars.value]];});
            op = 0.35 + ((d.data[vars.values[currVars.value]]/sum)*0.5);
          }
          return op;
          //arcvisible(d.current) ? (d.children ? 1 : 0.8) : 0;
        })
        .attr("d", d => arc(d.current))
        //.on("click", clicked)
        .on("mouseover", function(d) {
          let title = d.data.subfuncao || d.data.funcao;
          let titleStr = title.toUpperCase();
          let val, valStr, milStr;
          let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};

          if(d.data.subfuncao){
            val = d.data[vars.values[currVars.value]];
          }else{
            val = d3.sum(d.data.children, function(dd){
              return +dd[vars.values[currVars.value]];
            })
          }

          val = (val/1000000000).toFixed(1);

          milStr = val > 1 ? " bilhões": " bilhão";
          valStr = "R$ " + val + " " + milStr;
          tooltip.html(titleStr + "<span>" + valStr + "</span>");

          tooltip.classed("active", true);
          positionTooltip();
        })
        .on("mousemove", function(d) {
          positionTooltip();
        })
        .on("mouseout", function(){
          tooltip.classed("active", false)
        });

    path.filter(d => d.children)
      .style("cursor", "pointer")
      .attr("pointer-events", "all")

    parent = vis2.append("circle")
      .datum(nodes)
      .attr("r", radius)
      .attr("fill", "none")
      .attr("pointer-events", "all")
      .style("cursor", "pointer")

  }

  function positionTooltip(){
    let tW = d3.select("#tooltip").style("width").slice(0,-2);
    let tH = d3.select("#tooltip").style("height").slice(0,-2);
    let offset = {  x: tW/2, y: tH/2 }
    let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};
    pos.x = pos.x - offset.x;
    pos.y = pos.y - offset.y - 30;

    if(pos.x < 0){
      pos.x = 0;
    }else if(pos.x > window.innerWidth - (tW/2)){
      pos.x = window.innerWidth - (tW/2);
    }

    tooltip.style("left", pos.x+"px")
    .style("top", pos.y+"px");
  }

  function buildVis3(){
    svg.select("#budget1-vis3").remove();

    vis3 = svg.append("g")
      .attr("id", "budget1-vis3")
      .attr("opacity",0);

    if(!budgetData[currSelection]){
      //console.log("Não há dados disponíveis para a seleção de Ano/UF");
      return;
    }
    let nested = d3.nest()
    .key(function(d){
      return d.funcao;
    })
    .rollup(function(v){
      let obj = { uf: v[0].uf, ano: v[0].ano, funcao: v[0].funcao, subfuncao: v[0].subfuncao};
      for(var i=0; i<vars.values.length; i++){
        obj[vars.values[i]] = d3.sum(v, function(g) {return g[vars.values[i]]; }).toFixed(2);
      }
      return obj;
    })
    .entries(budgetData[currSelection])

    budgetByFunction = [];

    nested.forEach(function(d, i){
      budgetByFunction.push(d.value);
    });

    budgetByFunction.sort(function(a,b){
      return b.valor_atual - a.valor_atual;
    })


    let budgetBars = vis3.selectAll("g.gbar")
    .data(budgetByFunction)
    .enter()
    .append("g")
    .attr("cursor", "pointer")
    .attr("class", function(d){
      return "gbar gbar-"+ d.funcao.substr(0,1).toLowerCase();
    })
    .on("mouseover", function(d) {
      let title = d.funcao;
      let titleStr = title.toUpperCase();
      let val, valStr;
      let content = "";
      let milStr = "";

      if(currStep == 3){
        val = d.valor_loa;
        val = (val/1000000000).toFixed(2);
        milStr = val > 1 ? " bilhões": " bilhão";
        valStr = "<b>Previsto (LOA):</b> R$ " + val + " " + milStr + "<br/>";

        val = d.valor_emp;
        val = (val/1000000000).toFixed(2);
        milStr = val > 1 ? " bilhões": " bilhão";
        valStr += "<b>Executado:</b> R$ " + val + milStr;
      }else{
        val = d.valor_sup;
        val = (val/1000000).toFixed(2);
        // string de milhar singular
        milStr = val > 1 ? " milhões": " milhão";
        valStr = "<b>Suplementado:</b> R$ " + val + milStr;
      }

      tooltip.html(valStr);
      tooltip.classed("active", true);
      positionTooltip();
    })
    .on("mousemove", function(d) {
      positionTooltip();
    })
    .on("mouseout", function(){
      tooltip.classed("active", false)
    });

    let maxB = d3.max(budgetByFunction, function(d){
      return +d.valor_atual;
    });
    let maxB2 = d3.max(budgetByFunction, function(d){
      return ((+d.valor_loa) + (+d.valor_sup));
    });
    let minB = d3.min(budgetByFunction, function(d){
      return +d.valor_atual;
    });

    if(maxB2 > maxB){
      maxB = maxB2;
    }

    let xBarScale = d3.scaleLinear().domain([0, maxB]).range([0,(w-5)])
    let barH = (h-60)/10;
    let divisor = 1000000000;
    xAxis = d3.axisBottom().scale(xBarScale);
    xAxis.tickFormat(function(d){return (d/divisor).toFixed(1)});

    let gX = vis3.append("g")
      .attr("class", "axis axis-x")
      .attr("transform", "translate(0," + (barH*9) + ")")
      .call(xAxis);

    gX.append("text")
    .attr("class", "axis-title")
    .attr("x", 0)
    .attr("y", 35)
    .attr("fill", "#fff")
    .attr("text-anchor", "start")
    .text("R$ bilhões")

    /*budgetBars.append("rect")
    .attr("class", ".barAtual")
    .attr("rx", 4)
    .attr("ry", 4)
    .attr("x",vMargin)
    .attr("fill", function(d){
      //return "url(#diagC"+d.funcao.substr(0,1)+")";
      return colors[d.funcao];
    })
    .attr("fill-opacity", 0.35)
    .attr("width", function(d){
      return xBarScale(d.valor_atual);
    })
    .attr("height", barH*2)
    .attr("y", function(d,i){
      return (i*(barH*2.75));
    });*/


    budgetBars.append("rect")
    .attr("class", "barEmp")
    .attr("rx", 4)
    .attr("ry", 4)
    .attr("x",0)
    .attr("fill", function(d){
      return colors[d.funcao];//hashFills
    })
    .attr("width", function(d){
      return xBarScale(d.valor_emp);
    })
    .attr("height", barH)
    .attr("y", function(d,i){
      return (i*(barH*1.75));
    });


    budgetBars.append("rect")
    .attr("class", ".barLoa")
    .attr("rx", 0)
    .attr("ry", 0)
    .attr("fill", "url(#diagBar)")
    /*.attr("fill", function(d){
      return hashFills[d.funcao];//hashFills
    })*/
    .attr("fill-opacity",1)
    .attr("width", function(d){
      return xBarScale(d.valor_loa);
    })
    .attr("height", barH*0.7)
    .attr("x",0)
    .attr("y", function(d,i){
      return (i*(barH*1.75)) + barH*0.15;
    });

    /*
    budgetBars.append("line")
    .attr("class", "barLoa1")
    .attr("stroke", "#fff")
    .attr("stroke-width", 2)
    .attr("stroke-dasharray","4 0")
    .attr("y1", function(d,i){
      return (i*(barH*1.75));
    })
    .attr("y2", function(d,i){
      return (i*(barH*1.75)) + (barH);
    })
    .attr("x1", function(d){
      return 0 + xBarScale(d.valor_loa);
    })
    .attr("x2", function(d){
      return 0 + xBarScale(d.valor_loa);
    })
    budgetBars.append("line")
    .attr("class", "barLoa2")
    .attr("stroke", "#fff")
    .attr("stroke-width", 2)
    .attr("stroke-dasharray","4 0")
    .attr("y1", function(d,i){
      return (i*(barH*1.75)) + (barH/2);
    })
    .attr("y2", function(d,i){
      return (i*(barH*1.75)) + (barH/2);
    })
    .attr("x1", function(d){
      return 0;
    })
    .attr("x2", function(d){
      return 0 + xBarScale(d.valor_loa);
    })
    */



    budgetBars.append("rect")
    .attr("class", "barSup")
    .attr("rx", 5)
    .attr("ry", 5)
    .attr("fill", function(d){
      return hashFills[d.funcao];
    })
    .attr("fill-opacity", 0.9)
    .attr("width", 0)
    .attr("height", barH)
    .attr("x", function(d,i){
      let xPos = xBarScale(Math.abs(d.valor_emp));
      return xPos;
    })
    .attr("y", function(d,i){
      return (i*(barH*1.75));
    });

  }

  function labelvisible(d) {
    return d.y1 <= 3 && d.y0 >= 1 && (d.y1 - d.y0) * (d.x1 - d.x0) > 0.03;
  }

  function labelTransform(d) {
    const angleX = (d.x0 + d.x1) / 2;
    const angleY = (d.y0 + d.y1) / 2;

    const y = (radius*1.2) * Math.cos(angleX + (Math.PI/2));// * 180 / Math.PI
    const x = (radius*1.2) * Math.sin(angleX + (Math.PI/2));
    //return `rotate(${x - 90}) translate(${y},0) rotate(${x < 180 ? 0 : 180})`;

    return `translate(${x},${y})`;
  }

  function arcvisible(d) {
    return d.y1 <= 3 && d.y0 >= 1 && d.x1 > d.x0;
  }



  window.addEventListener("resize", function(){
    //svg.select(vis2)(remove);
    //svg.select(vis2)(remove);
    //d3.select("body").select("#tooltip").remove();
    //draw(currStep);
  });
  let initTimeout = setTimeout(setup, 200);

  return (
    <section id="budget">
    <div id="budget1-container" className="chart-block">
      <h3>Orçamento por função e subfunção
      <div id="budget1-selects" className="selector-group">
        <div id="bdg-select-states" className="select">
          <select>
            <option value="CE">CE</option>
            <option value="PR">PR</option>
            <option value="SP" defaultValue>SP</option>
          </select>
        </div>
        <div id="bdg-select-years" className="select">
          <select>
            <option value="2018" defaultValue>2018</option>
            <option value="2017">2017</option>
            <option value="2016">2016</option>
            <option value="2015">2015</option>
            <option value="2014">2014</option>
            <option value="2013">2013</option>
          </select>
        </div>
      </div>
      </h3>

      <div id="budget1-intro" className="intro-block">

        <div>
        <p className="active">Os demonstrativos de despesas orçamentárias do poder público são disponibilizados seguindo os padrões definidos pelo Tesouro Nacional. Nesse padrão, o orçamento é dividido em 28 funções. O <b>Justa</b> selecionou 5 funções de interesse para entender como os recursos são divididos entre elas: Direitos da Cidadania, Essencial à Justiça, Judiciária, Legislativa e Segurança Pública.<br/></p>
        </div>
        <ul id="budget-stepper" className="dot-stepper">
          <li><a id="last-budget" className="arrow-last"></a></li>
          <li className="active"><a className="dot"></a></li>
          <li><a className="dot"></a></li>
          <li><a className="dot"></a></li>
          <li><a className="dot"></a></li>
          <li><a id="next-budget" className="arrow-next pulse"></a></li>
        </ul>

        <div className="text-block">
          <p className="active">Das 28 funções em que é dividido o orçamento total, a parcela destinada às cinco funções citadas está representada no círculo menor</p>
          <p>Para cada função, há um conjunto de subfunções às quais será destinada cada fatia do orçamento. No gráfico, os valores destinados às subfunções aparecem no círculo externo</p>
          <p>O valor inicialmente previsto na Lei Orçamentária Anual (LOA) pode ser alterado ao longo do ano, para um valor maior ou menor</p>
          <p>A diferença é o chamado valor suplementado, representado a seguir para cada uma das cinco funções destacadas</p>
        </div>
        <ul id="budget1-leg" className="legenda">
          <li><span className="sq bdg-color1"></span> Segurança Pública</li>
          <li><span className="sq bdg-color2"></span> Legislativa</li>
          <li><span className="sq bdg-color3"></span> Judiciária</li>
          <li><span className="sq bdg-color4"></span> Essencial à Justiça</li>
          <li><span className="sq bdg-color5"></span> Direitos da Cidadania</li>
        </ul>
      </div>

      <div id="budget1-svg" className="svg-container no-scroll centered">
        <div id="bdg1-msg" className="prompt">
         <span>Não há dados disponíveis para a seleção de Ano/UF"</span>
        </div>
        <svg id="svg-budget-1" preserveAspectRatio="xMinYMin meet"  viewBox={"0 0 "+w+" "+h}>
          <defs>
          <pattern id="diagBar" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke="#000" strokeWidth="1.5" />
            <line x1="2.5" y="0" x2="2.5" y2="4" stroke="#fff" strokeWidth="1.5" />
          </pattern>
          <pattern id="diagCL" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke={ colors["LEGISLATIVA"] } strokeWidth="1.5" />
          </pattern>
          <pattern id="diagCJ" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke={ colors["JUDICIÁRIA"] } strokeWidth="1.5" />
          </pattern>
          <pattern id="diagCE" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke={ colors["ESSENCIAL À JUSTIÇA"] } strokeWidth="1.5" />
          </pattern>
          <pattern id="diagCS" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke={ colors["SEGURANÇA PÚBLICA"] } strokeWidth="1.5" />
          </pattern>
          <pattern id="diagCD" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke={ colors["DIREITOS DA CIDADANIA"] } strokeWidth="1.5" />
          </pattern>

          <pattern id="coloredLeg" patternUnits="userSpaceOnUse" width="2" height="20" patternTransform="">
          <rect x="0" y="0" width="2" height="3" fill={ colors["SEGURANÇA PÚBLICA"] } />
          <rect x="0" y="4" width="2" height="3" fill={ colors["JUDICIÁRIA"] } />
          <rect x="0" y="8" width="2" height="3" fill={ colors["DIREITOS DA CIDADANIA"] } />
          <rect x="0" y="12" width="2" height="3" fill={ colors["ESSENCIAL À JUSTIÇA"] } />
          <rect x="0" y="16" width="2" height="3" fill={ colors["LEGISLATIVA"] } />
          </pattern>

          <pattern id="hashLeg" patternUnits="userSpaceOnUse" width="2" height="20" patternTransform="">
          <rect x="0" y="0" width="2" height="3" fill={ colors["SEGURANÇA PÚBLICA"] } fillOpacity=".4" />
          <rect x="0" y="4" width="2" height="3" fill={ colors["JUDICIÁRIA"] } fillOpacity=".4" />
          <rect x="0" y="8" width="2" height="3" fill={ colors["DIREITOS DA CIDADANIA"] } fillOpacity=".4" />
          <rect x="0" y="12" width="2" height="3" fill={ colors["ESSENCIAL À JUSTIÇA"] } fillOpacity=".4" />
          <rect x="0" y="16" width="2" height="3" fill={ colors["LEGISLATIVA"] } fillOpacity=".4" />
          </pattern>

          <pattern id="lineLeg" patternUnits="userSpaceOnUse" width="15" height="15" patternTransform="">
            <rect x="0" y="6" width="15" height="2" fill="#fff" />
            <rect x="13" y="0" width="2" height="15" fill="#fff" />
          </pattern>

          </defs>
        </svg>
        <p className="chart-info">(Valores nominais, como informados pelo Poder Executivo Estadual)</p>
        <p className="chart-source"><b>Fonte:</b> Ceará - Estado do CE / Paraná - Estado do PR / São Paulo - Estado de SP
        <br/>
        <b>Elaboração:</b> JUSTA
        </p>
        <p>&nbsp;</p>
        <p>É importante entender alguns termos técnicos: <br/>
        <b>Valor Empenhado</b>: valores do orçamento que já foram comprometidos com determinada despesa <br/>
        <b>Lei Orçamentária Anual (LOA)</b>: a proposta de execução orçamentária do poder executivo aprovada pelo poder legislativo <br/>
        <b>Suplementação Orçamentária</b>: crédito adicionais abertos durante o ano</p>
      </div>
    </div>

    </section>
  )
}

export default Budget1;
