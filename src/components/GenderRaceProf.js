import * as d3 from "d3";
import React from 'react'
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';
import './../styles/Header.scss';

function GenderRaceProf(){
  let w = window.innerWidth - 20,
      h = window.innerHeight*0.8;
  let svg, x, y, svgH, svgW;
  let group1, group2;
  let margin = {top: 30, right: 20, bottom: 20, left: 40};
  let grData = {};
  let currStep = 1;
  let maxSteps = 4
  let colors = [
    "#F7D841",
    "#F7D841",
    "#D33145",
    "#D33145",
  ];
  let prefix = ["w","w","n","n"];
  let descriptions = [
    "Magistradas mulheres",
    "Desembargadoras mulheres",
    "Magistrados negros",
    "Desembargadores negros",
  ];
  let tooltip;

  function setup(){
    svg = d3.select("#svg-grp-1");

    // load data
    d3.tsv("data/gender-prof-uf.tsv", function(d,i,cols){
      for (i = 1; i < cols.length; ++i){
        d[cols[i]] = +d[cols[i]]
      }
      return d;
    })
    .then(function(data) {
      grData = data.sort(function(a,b){
        return b.nm - a.nm;
      });

      draw();
    })

    d3.selectAll("#next-grp,#last-grp").on("click", function(d){
      let isNotLast = currStep < maxSteps;
      let increment = 1;
      let initStep = 1;

      if(d3.select(this).attr("id")=="last-grp"){
        isNotLast = currStep > 1;
        increment = -1;
        initStep = maxSteps;
      }

      if(isNotLast){
        currStep += increment;
      }else{
        //currStep = initStep;
      }

      animateChange();

      let stepper = d3.select("#grp-stepper");
      stepper.style("opacity", 0.7);
      stepper.selectAll("li").each(function(d, i){
        if(i!=currStep){
          d3.select(this).classed("active", false);
          d3.select("#grp-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", false);
        }else{
          d3.select(this).classed("active", true);
          d3.select("#grp-intro").select(".text-block")
            .selectAll("p").filter(function(p,j){ return j == i-1 }).classed("active", true);
        }
        d3.select("#grp-intro").selectAll("ul").classed("active", false);
        d3.select("ul#grp-leg"+currStep).classed("active", true);
      })
    });



  }

  function showTooltip(d){
    //console.log(d);

    let value = '';
    let total = '';
    switch( currStep ){
        // Mulheres juízas
        case 1:
            value = '<b>Magistradas mulheres:</b> ' + Math.round(d.wm*1000)/10+"%";
            break;

        // Mulheres desembargadoras
        case 2:
            value = '<b>Desembargadoras:</b> ' + Math.round(d.wd*1000)/10+"%";
            total = '<b>Magistradas:</b> ' + Math.round(d.wm*1000)/10+"% <br/>";
            break;

        // Negros magistrados
        case 3:
            value = '<b>Magistrados negros:</b> ' + Math.round(d.nm*1000)/10+"%";
            break;

        // Negros desembargadores
        case 4:
            value = '<b>Desembargadores negros:</b> ' + Math.round(d.nd*1000)/10+"%";
            total = '<b>Magistrados negros:</b> ' + Math.round(d.nm*1000)/10+"%<br/>";
            break;

        default:
            break;
    }

    let data  = d;
    let str   = value;

    str += '<br/>' + total;

    tooltip.classed("active", true);
    tooltip.html(str);

    positionTooltip();
  }

  function draw(){
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip")
    }

    w = Number(svg.style("width").slice(0,-2));
    h = 27*26;

    svgH = h - margin.bottom - margin.top;
    svgW = w;
    /*
    h = window.innerHeight*0.8;
    if(window.innerHeight > window.innerWidth){
      h = window.innerHeight*0.6;
    }else if(h < 480){
      h = 480;
    }*/
    svg.attr("viewBox", "0 0 "+ w +" "+h);
    svg.html("");

    group1 = svg.append("g").attr("id", "grp-vis1")
      .attr("transform","translate(30,"+margin.top+")");
    group2 = svg.append("g").attr("id", "grp-vis2")
      .attr("transform","translate(30,"+margin.top+")");

    //group1.attr("transform", "translate(30,0)")

    x = d3.scaleLinear()
    .range([0,w-30]);

    y = d3.scaleBand()
    .rangeRound([0, svgH])
    .paddingInner(0.05)
    .align(0.1);

    let maxNm = [];
    maxNm[0] = d3.max(grData, function(d){
      return d.nm;
    });
    maxNm[1] = d3.max(grData, function(d){
      return d.nd;
    });
    //d3.max(maxNm)

    grData = grData.sort(function(a,b){
      return b[prefix[currStep-1]+"m"] - a[prefix[currStep-1]+"m"];
    });

    y.domain(grData.map(function(d) { return d.uf; }));
    x.domain([0, 1]);

    let bars = group1.selectAll("g.grp-bar")
      .data(grData)
      .enter().append("g")
      .attr("class","grp-bar")
      .on("mouseover", showTooltip)
      .on("mousemove", showTooltip)
      .on("mouseout", function(){
        tooltip.classed("active", false)
      });

    let barH = ((svgH-(grData.length*3))/grData.length)-2;

    bars.append("text")
    .attr("y", function(d) {
      return y(d.uf) + barH/2;
    })
    .attr("x", -30)
    .attr("fill", "#fff")
    .attr("font-size", "11px")
    .style("opacity", ".45")
    .text(function(d){
      return d.uf.toUpperCase();
    })

    bars.append("rect")
    .attr("class", "bg-grp1")
    .attr("rx","3").attr("ry","3")
    .attr("y","3")
    .attr("fill", "#fff")
    .attr("fill-opacity", .1)
    .attr("height", barH)
    .attr("width", function(d) {
      return x(1);
    })
    .attr("y", function(d) { return y(d.uf); })

    bars.append("rect")
    .attr("class", "bar-grp1")
    .attr("rx","3").attr("ry","3")
    .attr("y","3")
    .attr("fill", colors[currStep-1])
    .attr("height", barH)
    .attr("width", function(d) {
      return x(d.wm);
    })
    .attr("y", function(d) {
      return y(d.uf);
    })

    bars.append("rect")
    .attr("class", "bar-grp2")
    .attr("rx","3").attr("ry","3")
    .attr("y","3")
    .attr("fill", "url(#diagBar)")
    .attr("fill-opacity", .6)
    .attr("height", barH)
    .attr("y", function(d){ return y(d.uf); })
    .attr("width", 0)
    .attr("x", function(d){
      return x(d.wm);
    })

    let fiftyLine1 = svg.append("line")
    .attr("class","gr-line")
    .attr("x1", w/2)
    .attr("x2", w/2)
    .attr("y1", 15)
    .attr("y2", h-10)
    .attr("stroke","#dedede")
    .attr("stroke-dasharray","5 5")
    .attr("stroke-width","2")
    .attr("opacity",.5);

    let fiftyText = svg.append("text")
    .attr("class","gr-line")
    .attr("x", w/2 - 8)
    .attr("y", 10)
    .attr("fill","#fff")
    .style("font-size","10px")
    .style("font-family","Helvetica")
    .text("50%")
    .attr("opacity",.65);

  }

  function positionTooltip(){
    let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 40};

    let offset = {
        x: d3.select("#tooltip").style("width").slice(0,-2) / 2,
        y: d3.select("#tooltip").style("height").slice(0,-2) / 2
    }
    pos.x = pos.x - offset.x;
    pos.y = pos.y - offset.y;

    tooltip.style("left", pos.x+"px")
    .style("top", pos.y+"px");
  }

  function toPercentStr(v){
    let num = (v*100).toFixed(1);

    return num + "%";
  }

  function animateChange(){
    let index = currStep - 1;
    let barH = ((svgH-(grData.length*3))/grData.length)-2;
    let duration = 500;

    grData = grData.sort(function(a,b){
      return b[prefix[index]+"m"]-a[prefix[index]+"m"];
    })
    y.domain(grData.map(function(d) { return d.uf; }));

    group1.selectAll("text")
    .transition().duration(duration)
    .attr("y", function(d) {
      return y(d.uf) + barH/2;
    })

    group1.selectAll(".bg-grp1")
    .transition().duration(duration)
    .attr("y", function(d) {
      return y(d.uf);
    })

    if(index == 0 || index == 2){
      group1.selectAll(".bar-grp1")
      .transition().duration(duration)
      .attr("fill", colors[index])
      .attr("width", function(d) {
        return x(d[prefix[index]+"m"]);
      })
      .attr("y", function(d) {
        return y(d.uf);
      })

      group1.selectAll(".bar-grp2")
      .transition().duration(duration)
      .attr("width", 0)
      .attr("x", function(d) {
        return x(d[prefix[index]+"m"]);
      })
      .attr("y", function(d) {
        return y(d.uf);
      })
    }else if(index == 1 || index == 3){
      group1.selectAll(".bar-grp1")
      .transition().duration(duration)
      .attr("fill", colors[index])
      .attr("width", function(d) {
        return x(d[prefix[index]+"d"]);
      })
      .attr("y", function(d) {
        return y(d.uf);
      })

      group1.selectAll(".bar-grp2")
      .transition().duration(duration)
      .attr("width", function(d) {
        let diff = d[prefix[index]+"d"] - d[prefix[index]+"m"];
        let xDiff = x(Math.abs(diff))
        return xDiff;
      })
      .attr("x", function(d) {
        let diff = d[prefix[index]+"d"] - d[prefix[index]+"m"];
        let xDiff = x(d[prefix[index]+"m"]);
        if(diff < 0){
          xDiff = x(d[prefix[index]+"m"]) - x(Math.abs(diff));
        }
        return xDiff;
      })
      .attr("y", function(d) {
        return y(d.uf);
      })
    }
  }

  window.addEventListener("resize", function(){
    draw();
    animateChange();
  });
  let initTimeout = setTimeout(setup, 100);

  return (
      <div className="chart-block">
        <h3>Progressão na carreira: de juízes a desembargadores</h3>
        <div id="grp-intro" className="intro-block">
          <ul id="grp-stepper" className="dot-stepper">
            <li><a id="last-grp" className="arrow-last"></a></li>
            <li className="active"><a className="dot"></a></li>
            <li><a className="dot"></a></li>
            <li><a className="dot"></a></li>
            <li><a className="dot"></a></li>
            <li><a id="next-grp" className="arrow-next pulse"></a></li>
          </ul>
          <div className="text-block min-height">
            <p className="active">Em todos os estados, as mulheres são minoria entre os magistrados</p>
            <p>Na progressão da carreira, a desigualdade se acentua na maioria dos estados, com uma proporção menor de mulheres desembargadoras em quase todos os estados</p>
            <p>Somados homens e mulheres, magistrados autodeclarados negros são mais frequentes nas regiões Nordeste e Norte e não passam de 5% em estados como SC, RS e SP.</p>
            <p>Na maioria dos estados a desigualdade racial se acentua quando olhamos para os cargos de comando da instituição</p>

          </div>
          <ul id="grp-leg1" className="legenda active">
            <span className="title-desc block">Magistradas mulheres</span>
            <li><span className="sq ga"></span> Magistradas (% entre respondentes)</li>
          </ul>

          <ul id="grp-leg2" className="legenda">
            <span className="title-desc block">Desembargadoras mulheres</span>
            <li>
              <span className="sq ga"></span> Desembargadoras (% entre respondentes)
            </li>
            <li>
              <span className="sq g-dashed">
              <svg preserveAspectRatio="xMinYMin meet"  viewBox="0 0 12 12">
                <rect x="0" y="0" width="12" height="12" fill="url(#diagBar)"></rect>
              </svg>
              </span> Diferença entre mulheres magistradas e desembargadoras (% entre respondentes)
            </li>
          </ul>

          <ul id="grp-leg3" className="legenda">
            <span className="title-desc block">Magistrados negros</span>
            <li><span className="sq gn"></span> Magistrados negros (% entre respondentes)</li>
          </ul>

          <ul id="grp-leg4" className="legenda">
            <span className="title-desc block">Desembargadores negros</span>
            <li>
              <span className="sq gn"></span> Magistrados negros (% entre respondentes)
            </li>
            <li>
              <span className="sq g-dashed">
              <svg preserveAspectRatio="xMinYMin meet"  viewBox="0 0 12 12">
                <rect x="0" y="0" width="12" height="12" fill="url(#diagBar)"></rect>
              </svg>
              </span> Diferença entre magistrados e desembargadores negros (% entre respondentes)
            </li>
          </ul>

        </div>

        <svg id="svg-grp-1" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox={"0 0 "+w+" "+h+""}>
        </svg>

        <p className="chart-source">
        <b>Fonte:</b> CNJ 2018 e Censo IBGE 2010
        <br/>
        <b>Elaboração:</b> JUSTA
        </p>
      </div>
  )
}

/*
<li><span className="sq">
<svg preserveAspectRatio="xMinYMin meet" viewBox="0 0 12 12">
  <rect className="grp-leg grp-1 active" x="0" y="0" width="12" height="12" fill="#AE397C"></rect>
  <rect className="grp-leg grp-2" x="0" y="0" width="12" height="12" fill="#D33145"></rect>
</svg>
</span> Magistrados</li>
<li><span className="sq">
<svg preserveAspectRatio="xMinYMin meet" viewBox="0 0 12 12">
  <rect className="grp-leg grp-1 active" x="0" y="0" width="12" height="12" fill="url(#diagBar)" fillOpacity="0.5"></rect>
  <rect className="grp-leg grp-2" x="0" y="0" width="12" height="12" fill="url(#diagBar)" fillOpacity="0.5"></rect>
</svg>
</span> Desembargadores</li>
*/

export default GenderRaceProf;
