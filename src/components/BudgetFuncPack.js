import * as d3 from "d3";
import { sankey, sankeyLinkHorizontal, sankeyCenter } from "d3-sankey";
import React from 'react';
import Select from './custom.select';
//import Budget1B from './Budget1B';
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

function BudgetFunc(){
  let w = window.innerWidth,
      h = 400;
  let svg, vis;
  let margin = {top: 10, left:100, bottom: 25, right:60};
  let budgetData;
  let currVars = {state: 0, year: 0, value:3};
  let vars = {states: ["ce", "pr", "sp"],
              values: ["valor_loa", "valor_sup", "valor_atual", "valor_emp"],
              value_names: ["previsto", "suplementado", "atualizado", "empenhado"],
              years: ["2018","2017","2016","2015","2014","2013"]};
  let selectors;
  let currSelection;
  let chartH, chartW;
  let colors = {
    "LEGISLATIVA": "#3A8DA0",
    "JUDICIÁRIA": "#5AB755",
    "ESSENCIAL À JUSTIÇA": "#D33145",
    "SEGURANÇA PÚBLICA": "#F7D841",
    "DIREITOS DA CIDADANIA": "#AE397C"
  };
  let colorVal;
  let tooltip;
  let circles, nodes, clusters, simulation;
  let clusterNames = [];
  let maxV;

  function setup(){
    if(d3.select("#tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");
    }else{
      tooltip = d3.select("#tooltip")
    }

    let customSelects = document.querySelector("#budget1-selects").querySelectorAll(".select");

    selectors = [];
    customSelects.forEach(function(e,i){
      selectors.push( e );
    });

    svg = d3.select("#svg-budget-func");

    // load national gender data
    d3.tsv("data/orcamento-subfuncao.tsv", function(d,di,cols){
      for (var i = 0; i < cols.length; ++i){
        if(i > 3){ d[cols[i]] = +d[cols[i]]; }
      }
      return d;
    })
    .then(function(data) {
      // soma as linhas com a mesma "função - subfunção"
      let uniqueData = d3.nest()
        .key(function(d) {
          return d.ano+" - "+d.funcao+" - "+d.subfuncao+" - "+d.uf;
        })
        .rollup(function(v) {
          let obj = { uf: v[0].uf, ano: v[0].ano, funcao: v[0].funcao, subfuncao: v[0].subfuncao};
          for(var i=0; i<vars.values.length; i++){
            obj[vars.values[i]] = d3.sum(v, function(g) {return g[vars.values[i]]; }).toFixed(2);
          }
          return obj;
        }).entries(data);

      let unfiltered = [];
      uniqueData.forEach(function(d,i){
        unfiltered.push(d.value);
      });

      let filtered = d3.nest()
      .key(function(d){
        return d.uf.toLowerCase()+"_"+d.ano;
      })
      .entries(unfiltered);
      //
      //console.log(filtered);
      budgetData = {};
      filtered.forEach(function(d,i){
        budgetData[d.key] = d.values;
      });

      draw();
    });
  }

  function draw(redraw){
    w = d3.select("#budget2-svg-container").style("width").slice(0, -2);
    h = w * 0.65;
    //if(h < 420 || window.innerHeight > window.innerWidth){ h = 420; };
    if(w > window.innerHeight*.65){
      h = window.innerHeight*.65;
    };

    svg.attr("viewBox", "0 0 "+ w +" "+h);

    chartH = h - margin.top - margin.bottom;
    chartW = w - margin.left - margin.right;

    buildVis1();
  }

  function buildVis1(){
    currSelection = vars.states[currVars.state] +
                    "_"+vars.years[currVars.year];
    let ufData = budgetData[currSelection];
    /*.filter(function(d,i){
      return d.key == vars.states[currVars.state]+"_"+
                      vars.years[currVars.year];
    });*/
    console.log(ufData);

    let nest = d3.nest()
      .key(function(d) { return d.funcao; })
      .entries(ufData);
    clusters = []; // new Array(nest.length);

    nest.forEach(function(d,i){
      clusterNames.push(d.key);
    })

    maxV = d3.max(ufData, function(d,i){
      return +d.valor_emp;
    })

    nodes = [];

    ufData.forEach(function(d,i){
      let m = clusterNames.length-1;
      let r = Math.sqrt(d.valor_emp / maxV)*(h/3);
      let n = {
            cluster: clusterNames.indexOf(d.funcao),
            radius: r,
            data: d,
            x: (w/2) + ((i-(m-1))*60),//w/2 + ((i-(m/2)) * (w/m)), //Math.cos((i / m) * (2 * Math.PI)) * 100 + w / 2 + Math.random(),//w/2,//Math.cos(i / ufData.length * 2 * Math.PI) * 50 + (w/2),
            y: h/2 + (Math.random()*100) -50 //Math.sin((i / m) * (2 * Math.PI)) * 100 + h / 2 + Math.random() //Math.sin(i / ufData.length * 2 * Math.PI) * 50 + (h/2)
          };
      if (!clusters[i] || (r > clusters[i].radius)){
        clusters[i] = n;
      }
      nodes.push(n);
    });

    simulation = d3.forceSimulation()
    // keep entire simulation balanced around screen center
    .force('center', d3.forceCenter(w/2, h/2))
    // cluster by section
    .force('cluster', cluster().strength(.2))
    // apply collision with padding
    .force('collide', d3.forceCollide(d => d.radius + 4).strength(0.0))
    .on('tick', layoutTick)
    /*
    // keep entire simulation balanced around screen center
    .force('center', d3.forceCenter(w/2, h/2))
    .force("cluster", forceCluster)
    .force("gravity", d3.forceManyBody(20))
    // apply collision with padding
    .force('collide', d3.forceCollide(function (d) { return d.radius + 5; })
      .strength(0))

    .on('tick', layoutTick)*/
    .nodes(nodes);

    circles = svg.selectAll('circle')
    .data(nodes)
    .enter().append('circle')
      .attr("r", function (d) {
        let r = Math.sqrt(d.data.valor_emp / maxV)*(30)
        return r;
      })
      .style('fill', function (d) {
        //console.log(d.cluster);
        return colors[clusterNames[d.cluster]];
      })
      .on("mouseover", onMouseOver)
      .on("mousemove", moveTooltip)
      .on("mouseout", function(){
        tooltip.classed("active", false)
      })
      .call(d3.drag()
        .on('start', dragstarted)
        .on('drag', dragged)
        .on('end', dragended)
      );

      // ramp up collision strength to provide smooth transition
    let transitionTime = 1000;
    let t = d3.timer(function (elapsed) {
      let dt = elapsed / transitionTime;
      simulation.force('collide').strength(Math.pow(dt, 2) * 0.7);
      if (dt >= 1.0) t.stop();
    });

  }

  function onMouseOver(d){
    let title = "<b>" + d.data.subfuncao + "</b><br/>(FUNÇÃO: "+d.data.funcao + ")";
    let titleStr = title.toUpperCase();
    let val, valStr;
    let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};

    tooltip.classed("active", true);

    if(d.data.subfuncao){
      val = d.data[vars.values[currVars.value]];
    }else{
      val = d3.sum(d.data.children, function(dd){
        return +dd[vars.values[currVars.value]];
      })
    }

    val = (val/1000000000).toFixed(1);

    // string de milhar singular
    valStr = "R$ " + val+ ( val > 1 ? " bilhões": " bilhão" );
    tooltip.html(titleStr + "<span>" + valStr + "</span>");

    moveTooltip(d);
  }

  function moveTooltip(d){
    let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};
    let offset = {  x: d3.select("#tooltip").style("width").slice(0,-2)/2,
                    y: d3.select("#tooltip").style("height").slice(0,-2)/2}
    let tWidth = d3.select("#tooltip").style("width").slice(0,-2);
    pos.x = pos.x - offset.x;
    pos.y = pos.y - offset.y - 30;

    if(pos.x < 0){
      pos.x = 0;
    }else if(pos.x > window.innerWidth - 200){
      pos.x = window.innerWidth - 200;
    }

    tooltip.style("left", pos.x+"px")
    .style("top", pos.y+"px");
  }

  /*function forceCluster(alpha) {
    for(var i = 0, n = nodes.length, node, cluster, k = alpha * 1; i < n; ++i) {
      node = nodes[i];
      cluster = clusters[node.cluster];
      //console.log(node.cluster)
      node.vx -= (node.x - cluster.x) * k;
      node.vy -= (node.y - cluster.y) * k;
    }
  }*/

  function cluster () {

    var nodes,
      strength = 0.1;

    function force (alpha) {

      // scale + curve alpha value
      alpha *= strength * alpha;

      nodes.forEach(function(d) {
  			var cluster = clusters[d.cluster];
      	if (cluster === d) return;

        let x = d.x - cluster.x,
          y = d.y - cluster.y,
          l = Math.sqrt(x * x + y * y),
          r = d.radius + cluster.radius;

        if (l != r) {
          l = (l - r) / l * alpha;
          d.x -= x *= l;
          d.y -= y *= l;
          cluster.x += x;
          cluster.y += y;
        }
      });

    }

    force.initialize = function (_) {
      nodes = _;
    }

    force.strength = _ => {
      strength = _ == null ? strength : _;
      return force;
    };

    return force;

  }

  function layoutTick (e) {
    circles
      //.attr("cx", function(d) { return d.x; })
      //.attr("cy", function(d) { return d.y; })
      .attr("cx", function(d) { return d.x = Math.max(d.radius, Math.min(w - d.radius, d.x)); })
      .attr("cy", function(d) { return d.y = Math.max(d.radius, Math.min(h - d.radius, d.y)); })
      .attr('r', function (d) {
        //console.log(d);
        let r = Math.sqrt(d.data.valor_emp / maxV)*(30)
        return d.radius;
      });
  }

  function dragstarted (d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
  }

  function dragged (d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  function dragended (d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
  }

  function changeSelections(){
    //console.log(selectors);
    selectors.forEach(function(s,i){
      let sel = s.node;
      let selStr = sel.options[sel.selectedIndex].value;
      let list = sel.parentNode.getAttribute("id").split("-")[2];
      let index = vars[list].indexOf(selStr.toLowerCase());

      currVars[list.substr(0,list.length-1)] = index;
    })

    let tempSelection = vars.states[currVars.state];

    if(!budgetData[tempSelection]){
      let msg = "<span>Não há dados disponíveis para <b>"+vars.states[currVars.state].toUpperCase()+"</b>. Por favor, selecione outro estado</span>";
      d3.select("#bdg1-msg").html(msg).classed("active", true);
    }else{
      d3.select("#bdg1-msg").html("").classed("active", false);
      draw();
    }
  }


  window.addEventListener("resize", function(){

  });
  let initTimeout = setTimeout(setup, 200);

  return (
      <svg id="svg-budget-func" preserveAspectRatio="xMinYMin meet"  viewBox={"0 0 "+w+" "+h}>
        <defs>
          <pattern id="diagBar2" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
            <line x1="1" y="0" x2="1" y2="4" stroke="#666" fillOpacity="0.3" strokeWidth="1.5" />
            <line x1="2.5" y="0" x2="2.5" y2="4" stroke="#000" fillOpacity="0.5" strokeWidth="1.5" />
          </pattern>
        </defs>
      </svg>
  )
}
export default BudgetFunc;
