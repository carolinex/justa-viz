import * as d3 from "d3";
import React from 'react'
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';
import './../styles/Header.scss';

function GenderRace(){
  let w = window.innerWidth - 20,
      h = window.innerHeight*0.5;
  let svg;
  let groupM, groupF;
  let margin = {top: 20, right: 20, bottom: 30, left: 40};
  let grData = {};
  let descriptors = ["População", "Magistrados"];
  let x, y, z;
  let keys = [["bp","ip","ap","np"], ["bm","am","im","nm"]];
  let animInterval;
  let currAnim=0;
  let tooltip;

  function setup(){
    svg = d3.select("#svg-gr-1");

    // load national gender data
    d3.csv("data/gender-race-uf.csv", function(d,i,cols){
      var t1=0, t2=0;
      for (i = 2; i < cols.length; ++i){
        d[cols[i]] = +d[cols[i]]
      }
      for (i = 0; i < keys[0].length; ++i){
        t1 += Number(d[keys[0][i]]);
        t2 += Number(d[keys[1][i]]);
      }
      d.totals = [t1,t2];

      return d;
    })
    .then(function(data) {
      grData.f = data.filter(function(d,i){
        return d.sexo == "Mulheres";
      }).sort(function(a, b) { return a.bp - b.bp; });

      grData.m = data.filter(function(d,i){
        return d.sexo == "Homens";
      }).sort(function(a, b) { return a.bp - b.bp;} );

      draw();
    })
    .catch(function(error){
       console.log("error on gender-br");
    });
  }

  function draw(){
    tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "tooltip");

    w = Number(svg.style("width").slice(0,-2));
    h = window.innerHeight*0.65;
    if(window.innerHeight > window.innerWidth){
      h = window.innerHeight*0.6;
    }else if(h < 480){
      h = 480;
    }
    svg.attr("viewBox", "0 0 "+ w +" "+h);

    svg.html("");

    x = d3.scaleLinear()
    .range([0,(w-60)/2]);


    y = d3.scaleBand()
    .rangeRound([0, h])
    .paddingInner(0.05)
    .align(0.1);

    // set the colors
    z = d3.scaleOrdinal()
    .range(["#b33040", "#d25c4d", "#f2b447", "#d9d574"]);


    //console.log(grData.m.map(function(d) { return d.uf; }));
    y.domain(grData.m.map(function(d) { return d.uf; }));
    x.domain([0, 1]);
    z.domain(keys[0]);



    // stacked bars
    /*var groupM = svg.selectAll("g.gr-bar")
      .data(grData.m)
      .enter().append("g")
      .attr("class", "gr-bar");

    /*var rect = groupM.selectAll("rect")
      .data(function(d) { return d; })
      .enter()
      .append("rect")*/
    groupM = svg.append("g")
    var barsM = groupM.selectAll("g.gr-bar")
      .data(grData.m)
      .enter().append("g");

    barsM.append("text")
        .attr("x",0)
        .attr("y", function(d) {
          return y(d.uf) + ((h-(27*3))/27) - 5;
        })
        .attr("fill", "#fff")
        .attr("font-size", "11px")
        .style("opacity", "0.45")
        .text(function(d) {
          return d.uf;
        });

    // barras do gráfico - sexo masculino
    barsM.selectAll("rect")
      .data(function(d) {
        let a, b;
        let obj = [
                  {v:[d.np, d.nm], x:[0,0], uf:d.uf, label:"gn", totals:d.totals},
                  {v:[d.ip, d.im], x:[d.np,d.nm], uf:d.uf, label:"gi", totals:d.totals},
                  {v:[d.ap, d.am], x:[d.ip+d.np,d.im+d.nm], uf:d.uf, label:"ga", totals:d.totals},
                  {v:[d.bp, d.bm], x:[d.ip+d.np+d.ap,d.im+d.nm+d.am], uf:d.uf, label:"gb", totals:d.totals}
                ];

        return obj;
      })
      .enter().append("rect")
      .attr("class", function(d) {
        return "gr-rect "+d.label;
      })
      .attr("height", (h-(27*3))/27)
      .attr("width", function(d) {
        return x(d.v[0]/d.totals[0]);
      })
      .attr("data-v", function(d) {
        return d.v[0];
      })
      .attr("x", function(d) {
        //console.log(d.x[0]);
        return 25 + x(d.x[0]/d.totals[0]);
      })
      .attr("y", function(d) {
        return y(d.uf);
      })
      .on("mouseover", function(d){
        //console.log(d);
        let pos = {x: d3.event.pageX+20, y: d3.event.pageY - 30};
        tooltip.html("<span>test</span>");

        let offset = {  x: d3.select("#tooltip").style("width").slice(0,-2)/2,
                        y: d3.select("#tooltip").style("height").slice(0,-2)/2}
        pos.x = pos.x - offset.x;
        pos.y = pos.y - offset.y - 10;

        tooltip.style("left", pos.x+"px")
        .style("top", pos.y+"px");

        tooltip.classed("active", true);
      })
      .on("mouseout", function(){
        tooltip.classed("active", false)
      });

    groupF = svg.append("g")
    var barsF = groupF.selectAll("g.gr-bar")
      .data(grData.f)
      .enter().append("g");

    barsF.append("text")
        .attr("x", w)
        .attr("y", function(d) {
          return y(d.uf) + ((h-(27*3))/27) - 5;
        })
        .attr("fill", "#fff")
        .attr("text-anchor", "end")
        .attr("font-size", "11px")
        .style("opacity", "0.45")
        .text(function(d) {
          return d.uf;
        });

    barsF.selectAll("rect")
      .data(function(d) {
        let a, b;
        //console.log(d);
        let obj = [
                  {v:[d.np, d.nm], x:[d.ap+d.bp+d.ip,d.am+d.bm+d.im], uf:d.uf, label:"gn", totals:d.totals},
                  {v:[d.ip, d.im], x:[d.ap+d.bp,d.am+d.bm], uf:d.uf, label:"gi", totals:d.totals},
                  {v:[d.ap, d.am], x:[d.bp,d.bm], uf:d.uf, label:"ga", totals:d.totals},
                  {v:[d.bp, d.bm], x:[0,0], uf:d.uf, label:"gb", totals:d.totals}];

        return obj;
      })
      .enter().append("rect")
      .attr("class", function(d) {
        return "gr-rect "+d.label;
      })
      .attr("height", (h-(27*3))/27)
      .attr("width", function(d) {
        return x(d.v[0]/d.totals[0]);
      })
      .attr("x", function(d) {
        //console.log(d.x[0]);
        return (w/2) + 5 + x(d.x[0]/d.totals[0]);
      })
      .attr("y", function(d) {
        return y(d.uf);
      });

    clearInterval(animInterval);
    currAnim = 0;
    animInterval = setInterval(animateChange, 3000);
  }

  function animateChange(){
    if(currAnim==0){currAnim=1}else{currAnim=0}

    groupM.selectAll(".gr-rect")
    .transition().duration(500)
    .attr("width", function(d) {
      return x(d.v[currAnim]/d.totals[currAnim]);
    })
    .attr("x", function(d) {
      return 25 + x(d.x[currAnim]/d.totals[currAnim]);
    })

    groupF.selectAll(".gr-rect")
    .transition().duration(500)
    .attr("width", function(d) {
      return x(d.v[currAnim]/d.totals[currAnim]);
    })
    .attr("x", function(d) {
      return (w/2) + 5 + x(d.x[currAnim]/d.totals[currAnim]);
    })

    d3.select(".title-desc")
      .text(descriptors[currAnim]);
  }

  window.addEventListener("resize", function(){
    draw();
  });
  let initTimeout = setTimeout(setup, 100);

  return (
      <div className="chart-block">
        <h3 className="title-desc">População</h3>
        <p>Distribuição por cor e gênero nas Unidades Federativas do Brasil</p>

        <ul id="gr-leg1" className="legenda">
          <li><span className="sq gn"></span> Negros</li>
          <li><span className="sq gb"></span> Brancos</li>
          <li><span className="sq gi"></span> Indígenas</li>
          <li><span className="sq ga"></span> Amarelos</li>
        </ul>
        <svg id="svg-gr-1" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox={"0 0 "+w+" "+h+""}>

        </svg>
        <p>&nbsp;</p>
      </div>
  )
}

export default GenderRace;
