import React, { Component } from 'react';
import { ParallaxProvider } from 'react-scroll-parallax';
import ScrollMemory from 'react-router-scroll-memory';

import {
  HashRouter as Router,
  /*BrowserRouter as Router,*/
  Route,
  Link,
	Switch
} from 'react-router-dom';

// components
import Home from './pages/Home'
import './../styles/Header.scss';

// route config
const routes = [
	{
    path: "/",
    component: Home
  },
	{
    path: "/index.html",
    component: Home
  }
];

class App extends Component{

	render(){
    const baseUrl = process.env.PUBLIC_URL;
    const routeComponents = routes.map(({path, component}, key) => <Route exact path={path} component={component} key={key} />);
		return (
			<ParallaxProvider>
      <Router>
			  <div id="page-content">
        <ScrollMemory />
				{ routeComponents }
				</div>
			</Router>
      </ParallaxProvider>

		)
	}
}

export default App;
