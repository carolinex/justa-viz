import * as d3 from "d3";
import { sankey, sankeyLinkHorizontal, sankeyCenter } from "d3-sankey";
import React from 'react';
import Select from './custom.select';
//import Budget1B from './Budget1B';
//import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

function BudgetSup2(){
  let w = window.innerWidth,
      h = 400;
  let svg, vis;
  let margin = {top: 10, left:100, bottom: 25, right:60};
  let budgetData;
  let currVars = {state: 0, year: 0};
  let vars = {states: ["ce", "pr", "sp"],
              dept:["TRIBUNAL DE JUSTIÇA","DEFENSORIA PÚBLICA","MINISTÉRIO PÚBLICO"],
              years: [2018,2017,2016,2015,2014,2013,2012] };
  let selectors;
  let chartH, chartW;
  let colors = {
    "TRIBUNAL DE JUSTIÇA": "#3A8DA0",
    "DEFENSORIA PÚBLICA": "#5AB755",
    "MINISTÉRIO PÚBLICO": "#D33145"
  };
  let colorVal;
  let tooltip;
  let circles, nodes, clusters, simulation;
  let clusterNames = [];

  function setup(){
    if(d3.select("#budget1-tooltip").size() == 0){
      tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .attr("id", "budget1-tooltip");
    }

    let customSelects = document.querySelector("#budget2").querySelectorAll(".select");

    selectors = [];
    customSelects.forEach(function(e,i){
      selectors.push( new Select({target: e, callback: changeSelections}) );
    });

    svg = d3.select("#svg-budget-3");

    // load national gender data
    d3.tsv("data/orcamento-sup-orgao.tsv", function(d,di,cols){
      for (var i = 0; i < cols.length; ++i){
        if(i > 3){ d[cols[i]] = +d[cols[i]]; }
      }
      d.value = d.valor_sup;
      d.cluster = d.orgao;
      return d;
    })
    .then(function(data) {
      colorVal = d3.scaleLinear()
      .domain([0, 5])
      .range(["hsl(152,80%,80%)", "hsl(228,30%,40%)"])
      .interpolate(d3.interpolateHcl)

      budgetData = d3.nest()
        .key(function(d,i){
          return d.uf.toLowerCase()+"_"+d.ano;
        })
    	  //.key(function(d) { return d.orgao; })
    	  .entries(data);

      draw();
    });
  }

  function draw(step, redraw){
    w = d3.select("#budget2-svg-container").style("width").slice(0, -2);
    h = w * 0.65;
    //if(h < 420 || window.innerHeight > window.innerWidth){ h = 420; };
    if(w > window.innerHeight*.65){
      h = window.innerHeight*.65;
    };

    svg.attr("viewBox", "0 0 "+ w +" "+h);

    chartH = h - margin.top - margin.bottom;
    chartW = w - margin.left - margin.right;

    buildVis1();
  }

  function buildVis1(){
    let ufData = budgetData.filter(function(d,i){
      return d.key == vars.states[currVars.state]+"_"+
                      vars.years[currVars.year];
    })[0].values;
    console.log(ufData);

    let nest = d3.nest()
      .key(function(d) { return d.orgao; })
      .entries(ufData);
    clusters = []; // new Array(nest.length);

    nest.forEach(function(d,i){
      clusterNames.push(d.key);
    })

    let maxV = d3.max(ufData, function(d,i){
      return +d.valor_sup;
    })

    nodes = [];

    ufData.forEach(function(d,i){
      let r = Math.sqrt(d.valor_sup / maxV) * (w/4);
      let n = {
            cluster: clusterNames.indexOf(d.orgao),
            radius: r,
            x: w/2,//Math.cos(i / ufData.length * 2 * Math.PI) * 50 + (w/2),
            y: h/2//Math.sin(i / ufData.length * 2 * Math.PI) * 50 + (h/2)
          };
      if (!clusters[i] || (r > clusters[i].radius)){
        clusters[i] = n;
      }
      nodes.push(n);
    })

    console.log(clusters);

    simulation = d3.forceSimulation()
    // keep entire simulation balanced around screen center
    .force('center', d3.forceCenter(w/2, h/2))
    .force("cluster", forceCluster)
    .force("gravity", d3.forceManyBody(20))

    // pull toward center
    /*.force('attract', d3.forceAttract()
      .target([w/2, h/2])
      .strength(0.01))
    // cluster by section
    .force('cluster', d3.forceCluster()
      .centers(function (d) { return clusters[d.cluster]; })
      .strength(0.5)
      .centerInertia(0.1))
    */

    // apply collision with padding
    .force('collide', d3.forceCollide(function (d) { return d.radius + 5; })
      .strength(0))

    .on('tick', layoutTick)
    .nodes(nodes);

    circles = svg.selectAll('circle')
    .data(nodes)
    .enter().append('circle')
      .style('fill', function (d) {
        //console.log(d.cluster);
        return colors[clusterNames[d.cluster]];
      })
      /*.call(d3.drag()
        .on('start', dragstarted)
        .on('drag', dragged)
        .on('end', dragended)
      );*/

      // ramp up collision strength to provide smooth transition
    let transitionTime = 1000;
    let t = d3.timer(function (elapsed) {
      let dt = elapsed / transitionTime;
      simulation.force('collide').strength(Math.pow(dt, 2) * 0.7);
      if (dt >= 1.0) t.stop();
    });


/*
    let ufData = {
      key: 'root',
      values: budgetData.filter(function(d,i){
                return d.key == vars.states[currVars.state]+"_"+
                                vars.years[currVars.year];
              })
    };
    var hierarchy = d3.hierarchy(ufData, function(d) {
      return d.values;
    })
    .sum(d => d.value)
    .sort((a, b) => b.value - a.value);

    let pack = function(hData){
      return d3.pack()
      .size([w, h])
      .padding(3)
      (hData)
    }


    let root = pack(hierarchy);
    focus = root;


    var node = svg.selectAll("g")
    .data(root.descendants())
    .enter().append("g")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
      .attr("class", function(d) { return "node" + (!d.children ? " node--leaf" : d.depth ? "" : " node--root"); })
      .each(function(d) { d.node = this; });

    node.append("circle")
        .attr("id", function(d,i) { return "node-" + i; })
        .attr("r", function(d) { return d.r; })
        .attr("fill", function(d){
          let fill;// = "rgba(30,30,35,.5)";
          //console.log(d);
          fill =  d.children ? "rgba(30,30,35,.5)" : colors[d.data.orgao];
          if( d.data.key == "root" ||
              (d.data.key && d.data.key.indexOf("_") > -1)){
            fill = "rgba(60,60,65,.35)"
          }
          return fill;
        })*/
  }

  function forceCluster(alpha) {
    for (var i = 0, n = nodes.length, node, cluster, k = alpha * 1; i < n; ++i) {
      node = nodes[i];
      cluster = clusters[node.cluster];
      //console.log(node.cluster)
      node.vx -= (node.x - cluster.x) * k;
      node.vy -= (node.y - cluster.y) * k;
    }
  }

  function layoutTick (e) {
    circles
      .attr("cx", function(d) { return d.x = Math.max(d.radius, Math.min(w - d.radius, d.x)); })
      .attr("cy", function(d) { return d.y = Math.max(d.radius, Math.min(h - d.radius, d.y)); })
      .attr('r', function (d) { return d.radius; });
  }

  function dragstarted (d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
  }

  function dragged (d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  function dragended (d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
  }

  function changeSelections(){
    //console.log(selectors);
    selectors.forEach(function(s,i){
      let sel = s.node;
      let selStr = sel.options[sel.selectedIndex].value;
      let list = sel.parentNode.getAttribute("id").split("-")[2];
      let index = vars[list].indexOf(selStr.toLowerCase());

      currVars[list.substr(0,list.length-1)] = index;
    })

    let tempSelection = vars.states[currVars.state];

    if(!budgetData[tempSelection]){
      let msg = "<span>Não há dados disponíveis para <b>"+vars.states[currVars.state].toUpperCase()+"</b>. Por favor, selecione outro estado</span>";
      d3.select("#bdg1-msg").html(msg).classed("active", true);
    }else{
      d3.select("#bdg1-msg").html("").classed("active", false);
      draw();
    }
  }

  function rightRoundedRect(x, y, width, height, radius) {
    return "M" + x + "," + y
       + "v" + (-radius - height)
       + "a" + radius + "," + (-radius) + " 1 0 1 " + radius + "," + (-radius)
       + "h" + (width - radius)
       + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
       + "v" + (height + radius)
       + "z";
  }

  window.addEventListener("resize", function(){

  });
  let initTimeout = setTimeout(setup, 200);

  return (
    <svg id="svg-budget-3" preserveAspectRatio="xMinYMin meet"  viewBox={"0 0 "+w+" "+h}>
      <defs>
        <pattern id="diagBar2" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(-45)">
          <line x1="1" y="0" x2="1" y2="4" stroke="#666" fillOpacity="0.3" strokeWidth="1.5" />
          <line x1="2.5" y="0" x2="2.5" y2="4" stroke="#000" fillOpacity="0.5" strokeWidth="1.5" />
        </pattern>
      </defs>
    </svg>
  )
}
export default BudgetSup2;
