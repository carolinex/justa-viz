import * as d3 from "d3";
import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

/* components */
import Header from './../Header';
import Gender from './../Gender';
import Egress from './../Egress';
import Budget from './../Budget';
import BudgetSup from './../BudgetSup';
import Suspension from './../Suspension';

class Home extends Component{
	render(){
		return (
			<div id="main">
			<div className="bg-image"></div>
			<Helmet>
				<title>Justa</title>

				<meta property="og:url" content="http://justa.org.br/acesse-os-dados/" />
				<meta property="og:image" content="http://justa.org.br/dados/images/justica_b.jpg" />
				<meta property="og:image:type" content="image/jpg" />
				<meta property="og:site_name" content="Justa" />
				<meta property="og:title" content="Justa" />
				<meta property="og:description" content="Projeto de pesquisa que se propõe a facilitar o entendimento e a visualização de dados do financiamento e da gestão do Sistema de Justiça" />
				<meta property="og:type" content="website" />
				<meta property="og:locale" content="pt_BR" />

			</Helmet>
			<Header></Header>
			<Gender></Gender>
			<Egress></Egress>
			<Budget></Budget>
			<BudgetSup></BudgetSup>
			<Suspension></Suspension>
		</div>
		)
	}
}

export default Home;
